# Kolla terraform

Use terraform Libvirt module to deploy an OpenStack lab cluster

Usage:
- Deploy KVM virtualization host (RHEL, Debian, whatever)
- Install terraform-libvirt
- Adjust main module values for host numbers, role and capacity
- terraform init 
- terraform apply
