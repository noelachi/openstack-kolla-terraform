variable "dns_domain" {
  description = "DNS domain name"
  default     = "homelab.nac"

}

variable "ctrl_count" {
  description = "number of virtual-machine of same type that will be created"
  default     = 3
}

variable "cmp_count" {
  description = "number of virtual-machine of same type that will be created"
  default     = 3
}

variable "storage_count" {
  description = "number of virtual-machine of same type that will be created"
  default     = 3
}

variable "kube_count" {
  description = "number of virtual-machine of same type that will be created"
  default     = 1
}

variable "memory" {
  description = "The amount of RAM (MB) for a node"
  default     = 8192
}

variable "vcpu" {
  description = "The amount of virtual CPUs for a node"
  default     = 2
}
