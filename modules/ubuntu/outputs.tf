output "ubuntu_18_id" {
  value = libvirt_volume.ubuntu_18.id
}

output "ubuntu_20_id" {
  value = libvirt_volume.ubuntu_20.id
}

output "ubuntu_20_kvm_id" {
  value = libvirt_volume.ubuntu_20_kvm.id
}