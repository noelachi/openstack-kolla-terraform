terraform {
  required_version = ">= 0.13"
  required_providers {
    libvirt = {
      source  = "dmacvicar/libvirt"
      version = "0.6.3"
    }
  }
}

resource "libvirt_volume" "ubuntu_18" {
  name   = "ubuntu_18"
  source = "${var.images_dir}/bionic-server-cloudimg-amd64.img"
  pool   = var.pool
}

resource "libvirt_volume" "ubuntu_20" {
  name   = "ubuntu_20"
  source = "${var.images_dir}/focal-server-cloudimg-amd64.img"
  pool   = var.pool
}

resource "libvirt_volume" "ubuntu_20_kvm" {
  name   = "ubuntu_20_kvm"
  source = "${var.images_dir}/focal-server-cloudimg-amd64-disk-kvm.img"
  pool   = var.pool
}

