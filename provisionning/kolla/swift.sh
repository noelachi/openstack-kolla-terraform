#!/bin/bash

#yum  install -y epel-release
#yum install -y yum-utils device-mapper-persistent-data lvm2
#yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
#yum install -y docker-ce docker-ce-cli containerd.io --nogpgcheck 
BASE_DIR="$HOME/openstack-kolla-terraform/provisionning/kolla/config/swift/"

STORAGE_NODES=(172.16.31.45 172.16.31.46 172.16.31.47)
KOLLA_SWIFT_BASE_IMAGE="kolla/oraclelinux-source-swift-base:4.0.0"
rm -rf $BASE_DIR
mkdir -p $BASE_DIR

docker run \
  --rm \
  -v $BASE_DIR:/etc/kolla/config/swift/ \
  $KOLLA_SWIFT_BASE_IMAGE \
  swift-ring-builder \
    /etc/kolla/config/swift/object.builder create 10 3 1

for node in ${STORAGE_NODES[@]}; do
    for i in {1..3}; do
      docker run \
        --rm \
        -v $BASE_DIR:/etc/kolla/config/swift/ \
        $KOLLA_SWIFT_BASE_IMAGE \
        swift-ring-builder \
          /etc/kolla/config/swift/object.builder add r1z$i-${node}:6000/D${i} 60;
    done
done


docker run \
  --rm \
  -v $BASE_DIR:/etc/kolla/config/swift/ \
  $KOLLA_SWIFT_BASE_IMAGE \
  swift-ring-builder \
    /etc/kolla/config/swift/account.builder create 10 3 1

for node in ${STORAGE_NODES[@]}; do
    for i in {1..3}; do
      docker run \
        --rm \
        -v $BASE_DIR:/etc/kolla/config/swift/ \
        $KOLLA_SWIFT_BASE_IMAGE \
        swift-ring-builder \
          /etc/kolla/config/swift/account.builder add r1z$i-${node}:6001/D${i} 60;
    done
done



docker run \
  --rm \
  -v $BASE_DIR:/etc/kolla/config/swift/ \
  $KOLLA_SWIFT_BASE_IMAGE \
  swift-ring-builder \
    /etc/kolla/config/swift/container.builder create 10 3 1

for node in ${STORAGE_NODES[@]}; do
    for i in {1..3}; do
      docker run \
        --rm \
        -v $BASE_DIR:/etc/kolla/config/swift/ \
        $KOLLA_SWIFT_BASE_IMAGE \
        swift-ring-builder \
          /etc/kolla/config/swift/container.builder add r1z$i-${node}:6002/D${i} 60;
    done
done


for ring in object account container; do
  docker run \
    --rm \
    -v $BASE_DIR:/etc/kolla/config/swift/ \
    $KOLLA_SWIFT_BASE_IMAGE \
    swift-ring-builder \
      /etc/kolla/config/swift/${ring}.builder rebalance;
done

#sudo chown -R luser:luser /etc/kolla