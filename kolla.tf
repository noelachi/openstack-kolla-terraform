#networks
resource "libvirt_network" "storage_net" {
  # the name used by libvirt
  name = "storage_net"
  mode = "none"
  addresses = ["172.16.31.0/24"]
  autostart = true
  dns {
    enabled = false
  }
  dhcp {
		enabled = false
	}
}

#networks
resource "libvirt_network" "external_net" {
  name = "external_net"
  mode = "nat"
  domain = "external.local"
  addresses = ["172.16.32.0/24"]
  autostart = true
  # (Optional) DNS configuration
  dns {
    enabled = true
   local_only = true
   forwarders {
       address = "8.8.8.8"
       domain = "external.local"
    } 
  }
  dhcp {
		enabled = false
	}
}

module "cloudinit" {
  source             = "./modules/cloudinit/"
  cloudinit_name        = "centos8_cloudinit.iso"
  cloudinit_filename = "cloud_init_resize.cfg"
}

module "centos" {
  source = "./modules/centos/"
}

module "ubuntu" {
  source = "./modules/ubuntu/"
}

resource "libvirt_volume" "ctrl_os_disk" {
  name           = "ctrl_os_disk-${count.index}"
  base_volume_id = module.centos.centos_8_2_id
  count          = var.ctrl_count 
  pool = "nvme"
  format = "qcow2"
  size = "42949672960" #40GB
}

resource "libvirt_volume" "cmp_os_disk" {
  name           = "cmp_os_disk-${count.index}"
  base_volume_id = module.centos.centos_8_2_id
  count          = var.cmp_count
  pool = "nvme"
  format = "qcow2"
  size = "42949672960" #40GB
}

resource "libvirt_volume" "storage_os_disk" {
  name           = "storage_os_disk-${count.index}"
  base_volume_id = module.centos.centos_8_2_id
  count          = var.storage_count
  pool = "nvme"
  format = "qcow2"
  size = "42949672960" #40GB
}

resource "libvirt_volume" "kube_os_disk" {
  name           = "kube_os_disk-${count.index}"
#  base_volume_id = module.ubuntu.ubuntu_20_kvm_id #ubuntu based kube node
  base_volume_id = module.centos.centos_8_2_id #log node
  count          = var.kube_count
  pool = "default"
  format = "qcow2"
  size = "773094113280" #720GB
}



#resource "libvirt_volume" "kube_data_disk" {
#  name           = "kube_os_disk-${count.index}"
#  count          = var.kube_count
#  pool = "sshd"
#  format = "qcow2"
#  size = "687194767360" #640GB
#}

resource "libvirt_volume" "nova_disk" {
  name           = "nova_disk-${count.index}"
  count          = var.cmp_count
  pool = "nvme"
  format = "qcow2"
  size = "214748364800" #200GB
}

resource "libvirt_volume" "storage_cinder" {
  name           = "storage_cinder-${count.index}"
  count          = var.storage_count
  pool = "sshd"
  format = "qcow2"
  size = "408021893120" #380GB
}

resource "libvirt_volume" "storage_obj_a" {
  name           = "storage_obj_a-${count.index}"
  count          = var.storage_count
  pool = "sshd"
  format = "qcow2"
  size = "64424509440" #60GB
}

resource "libvirt_volume" "storage_obj_b" {
  name           = "storage_obj_b-${count.index}"
  count          = var.storage_count
  pool = "sshd"
  format = "qcow2"
  size = "64424509440" #60GB
}

resource "libvirt_volume" "storage_obj_c" {
  name           = "storage_obj_c-${count.index}"
  count          = var.storage_count
  pool = "sshd"
  format = "qcow2"
  size = "64424509440" #60GB
}

resource "libvirt_domain" "control" {
  name      = "control-${count.index}"
  memory    = "8192"
  vcpu      = 2
  count     = var.ctrl_count
  cloudinit = module.cloudinit.cloudinit_id
  cpu = {
    mode = "host-passthrough"
  }
  autostart = true

  network_interface {
    network_name = "br0"
    hostname = "ctrl0${count.index +1}.${var.dns_domain}"
    mac = "52:54:00:c0:6a:3${count.index +1}"
  }
  network_interface {
    network_name = "storage_net"
  }
  network_interface {
    network_name = "external_net"
  }
  // OS image
  disk {
    volume_id = element(libvirt_volume.ctrl_os_disk.*.id, count.index)
  }

  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }

  console {
    type        = "pty"
    target_type = "virtio"
    target_port = "1"
  }

  graphics {
    type        = "spice"
    listen_type = "address"
    autoport    = true
  }

}

resource "libvirt_domain" "compute" {
  name      = "compute-${count.index}"
  memory    = "18432"
  vcpu      = 8
  count     = var.cmp_count
  cloudinit = module.cloudinit.cloudinit_id
  cpu = {
    mode = "host-passthrough"
  }
  autostart = true

  network_interface {
    network_name = "br0"
    hostname = "cmp0${count.index +1}.${var.dns_domain}"
    mac = "52:54:00:ce:b0:c${count.index +1}"
  }
  network_interface {
    network_name = "storage_net"
  }
  network_interface {
    network_name = "external_net"
  }
  // OS image
  disk {
    volume_id = element(libvirt_volume.cmp_os_disk.*.id, count.index)
  }
  disk {
    volume_id = element(libvirt_volume.nova_disk.*.id, count.index)
  }

  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }

  console {
    type        = "pty"
    target_type = "virtio"
    target_port = "1"
  }

  graphics {
    type        = "spice"
    listen_type = "address"
    autoport    = true
  }

}

resource "libvirt_domain" "storage" {
  name      = "storage-${count.index}"
  memory    = "4096"
  vcpu      = 1
  count     = var.storage_count
  cloudinit = module.cloudinit.cloudinit_id
  cpu = {
    mode = "host-passthrough"
  }
  autostart = true

  network_interface {
    network_name = "br0"
    hostname = "storage0${count.index +1}.${var.dns_domain}"
    mac = "52:54:00:d8:02:e${count.index +1}"
  }
  network_interface {
    network_name = "storage_net"
  }
  // OS image
  disk {
    volume_id = element(libvirt_volume.storage_os_disk.*.id, count.index)
  }
  disk {
    volume_id = element(libvirt_volume.storage_cinder.*.id, count.index)
  }
  disk {
    volume_id = element(libvirt_volume.storage_obj_a.*.id, count.index)
  }
  disk {
    volume_id = element(libvirt_volume.storage_obj_b.*.id, count.index)
  }
  disk {
    volume_id = element(libvirt_volume.storage_obj_c.*.id, count.index)
  }

  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }

  console {
    type        = "pty"
    target_type = "virtio"
    target_port = "1"
  }

  graphics {
    type        = "spice"
    listen_type = "address"
    autoport    = true
  }

}

resource "libvirt_domain" "kube" {
  name      = "kube-${count.index}"
  memory    = "8192"
  vcpu      = 2
  count     = var.kube_count
  cloudinit = module.cloudinit.cloudinit_id
  cpu = {
    mode = "host-passthrough"
  }
  autostart = true

  network_interface {
    network_name = "br0"
    hostname = "kube0${count.index +1}.${var.dns_domain}"
    mac = "52:54:00:20:e9:8${count.index +1}"
  }
  network_interface {
    network_name = "storage_net"
  }
  // OS image
  disk {
    volume_id = element(libvirt_volume.kube_os_disk.*.id, count.index)
  }

  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }

  console {
    type        = "pty"
    target_type = "virtio"
    target_port = "1"
  }

  graphics {
    type        = "spice"
    listen_type = "address"
    autoport    = true
  }

  depends_on = [libvirt_domain.compute, libvirt_domain.storage, libvirt_domain.control]

  provisioner "local-exec" {
    command = "sleep 3m; ansible-playbook -u luser -i provisionning/inventory/ provisionning/site.yml"
  }

}

